## Énoncé du brief

Nous sommes en 2248, les terriens ont tous migré sur Mars, faute d'avoir pu sauver la Terre. A cette époque, chacun se déplace en combinaison étanche à l'atmosphère et aux radiations du soleil. Seules quelques zones de radiations nucléaires liées à quelques minerais endémiques de la planète mettent à mal les personnes et leurs équipements de survie, équipés toutefois d'un détecteur de zones sensibles, et d'un avertisseur sonore.

Afin de mettre fin à l'écatombe qui se produit actuellement dans la population néomarsienne, le Président Directeur Mondial vous impose d'assurer la mission suivante, impérativement sous 3 jours : créer une application qui permette à chacun de visualiser les zones sensibles à éviter. Comme personne n'a encore cartographié ces territoires, il faudra prévoir que chaque habitant puisse ajouter des zones, de manière collaborative...

3 minerais sont à distinguer, par ordre de dangerosité : 

Le Klingon, un peu relou, altère les combinaisons, traverse les matériaux et provoque des démangeaisons cutanées,  On a observé une résistance chez les geeks 
Le Chomdû est un minerai qui provoque des états dépressifs. Bizzarement, les personnes âgées ne sont plus affectées... 
Enfin, le Perl est le minerai à éviter à tout prix ! Hystérie, dépression, folie, voir mort subite sont les effets fréquemment observés.
Chaque zone peut être qualifiée par un niveau de dangerosité de 1 à 10, lié notamment à la quantité de minerai. Il est aussi important de bien enregistrer la date de cartographie pour que nos scientifiques puissent étudier l'évolution de ces phénomènes. Prévoyez en outre que de nouveaux minerais pourraient encore être découverts ! 

Bon courage...

PS : Les technologies ayant évolué vers une intelligence artificielle malsaine appelée Skynet, une mode néo-rétro a remis au goût du jour les technologies en usage sur Terre au 21è siècle (HTML, CSS, Javascript, PHP...)

## Livrables attendus
Merci de rendre obligatoirement avec le projet : 

- les cas d'utilisation ou les scénarios utilisateurs: [Taïga.io](https://tree.taiga.io/project/thomasreveldev-simplonmars/backlog)
- les maquettes correspondant aux documents précédents, de qualité professionnelle (les documents scannés ne seront pas validés) -> **voir dans le dossier UML-Wireframes: partie 1 et 2**
- le kanban : [Taïga.io](https://tree.taiga.io/project/thomasreveldev-simplonmars/backlog)
- le backlog : [Taïga.io](https://tree.taiga.io/project/thomasreveldev-simplonmars/backlog)
- le schéma de base de données (format au choix, mais standardisé) -> **voir dans le dossier UML-Wireframes : UML Class Diagram**
- le dépôt GIT (on attend des commit réguliers et quotidiens) : [GitLab](https://gitlab.com/thomas314/simplonmars/tree/master)
- l'url de préprod du projet fonctionnel

Tous les documents seront mis à disosition dans un dossier dédié dans le dépôt GIT (pas de lien vers une source externe à l'exception de la préprod)


## Pour lancer le projet en local:

1. Cloner/télécharger le projet,

Depuis le dossier projet, écrire dans votre terminal:


```composer install && npm install```  (installation des dépendances nécessaires au fonctionnement des divers modules)


2. Avec un SGBD, type "MySQL Workbench", créer une BDD (connexion & schema) vide pour l'instant.


Dans notre projet, le fichier **.env** stocke toutes les données sensibles de notre application dans des variables d'environnement.
Il ne peut et ne doit donc en aucun cas être mis en ligne et doit être créé localement.

Dupliquer le .env.exemple, renommer la copie en .env, puis y renseigner nos informations.

Pour accèder à votre base de données, il faut renseigner :
```DB_DATABASE= *nom-de-la-BDD*```
```DB_USERNAME= *nom-utilisateur-BDD*```
```DB_PASSWORD= *mdp-BDD*```

3. Il faut également définir **la clés d'application, l'APP_KEY,** cela se fait automatiquement avec la commande :


```php artisan key:generate``` (toujours depuis le dossier projet)


4. On peut désormais initialiser les tables de la BDD:

```php artisan migrate```

5. À ce stade, la base de donnée est prête et on peut lancer notre serveur :
```php artisan serve```

6. Cependant, pour gérer **l'administration du site**,

il va falloir **installer & configurer Voyager**.

## Voyager
La mise en place notre tableau de bord administrateur nécessite l'installation de ***Voyager*** :
```php artisan voyager:install```


On va maintenant créer **notre premier administrateur** avec :
```php artisan voyager:admin your@email.com --create``` (pensez à modifier l'email renseigné)

Vous êtes invité à renseigner **un nom (votre pseudo)** et un **mot de passe (permettant la connexion au tableau de bord admin)** .


On peut maintenant lancer le serveur local :
```php artisan serve```

Ouvrir l'URL : ```http://localhost:8000```


Connectez vous avez vos identifiants administrateur

Puis ajouter ```/admin``` à la suite de l'adresse locale.


Vous serez **redirigé sur la plateforme de Voyager** et devrez **vous connecter avec le mail et le mot de passe renseigné** auparavant.

**VOUS VOILA SUR VOTRE TABLEAU DE BORD ADMINISTRATEUR ! **

**NOTE DÉVELOPPEUR**

À ce stade de développement, une **erreur persiste au sein de Voyager**. 

Pour accèder à l'onglet **DATABASE dans le menu déroulant Tools**,
une solution est d'indiquer **l'id de l'utilisateur dans un fichier Vendor** de voyager.

Dans le fichier 
```vendor/tcg/voyager/resources/views/tools/database/index.blade.php```
(à la ligne 96)

remplacez: 
```['id' => null] par ['id' => Auth::user()->id]```