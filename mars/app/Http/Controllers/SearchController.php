<?php

namespace App\Http\Controllers;

use App\Minerai;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index()
    {
        return view('recherche');
    }
    public function findSearch()
    {
        $search = request()->input("search");
        $mineral = Minerai::where('name', 'LIKE', '%' . $search . '%')->get();
        if (count($mineral) > 0){
            return view('recherche', ["minerai" => $mineral])->withDetails($mineral)->withQuery($search);
        }
        else
            return view('recherche')->withMessage('"La recherche associée à '. $search . ' n\a pas aboutie');
    }
}
