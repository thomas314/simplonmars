<?php

namespace App\Http\Controllers;

use App\Minerai;
use Illuminate\Http\Request;


class MineraiController extends Controller
{
    public function liste()
    {
        $minerals = Minerai::all();
        return view('minerals', compact('minerals')); 
    }
    public function add_minerals(Request $request)
    {
        $minerals = new Minerai;
        $minerals->name = $request->input('name');
        $minerals->resume = $request->input('resume');
        $minerals->dangerosity = $request->input('dangerosity');
        $minerals->save();

        // dd($minerals);
        return redirect('minerals');
    }
}
