<?php

namespace App\Http\Controllers;

use App\Zone;
use Illuminate\Http\Request;

class ZoneController extends Controller
{
    public function liste()
    {
        $zones = Zone::all();
        return view('zones', compact('zones')); 
    }
    public function add_zones(Request $request)
    {
        $zones = new Zone;
        // $zone->title = request('title');
        // $zone->resume = request('resume');
        // $zone->dangerosity = request('dangerosity');
        $zones->title = $request->input('title');
        $zones->resume = $request->input('resume');
        $zones->dangerosity = $request->input('dangerosity');
        $zones->save();

        // dd($zones);
        return redirect('zones');
    }
}
