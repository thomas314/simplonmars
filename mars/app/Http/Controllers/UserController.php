<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\User;
use TCG\Voyager\Models\User as ModelsUser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return view('param');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ProfilRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $users = User::find(Auth::user()->id);
        $users->name = $request->name;
        $users->email = $request->email;
        $users->save();
        return redirect('param');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = Auth::user()->id;
        $users = User::findOrFail($id);
        $users->delete();

        $request->session()->flush();

        return redirect('login');
    }
}
