<?php

// use Symfony\Component\Routing\Route;
// use Illuminate\Routing\Route;
// use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\Route as FacadesRoute;
// use TCG\Voyager\Facades\Voyager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/recherche', 'SearchController@index');
    Route::get('/maps', 'MapController@index');
    Route::get('/param', 'UserController@index');
    Route::get('/zones', 'ZoneController@liste');
    Route::get('/minerals', 'MineraiController@liste');

    Route::post('/add_zones', 'ZoneController@add_zones');
    Route::post('/add_minerals', 'MineraiController@add_minerals');
    Route::post('/param_user', 'UserController@update');
    Route::post('/del_user', 'UserController@destroy');
    Route::post('/findSearch', 'SearchController@findSearch');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
