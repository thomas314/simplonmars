<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Mars 2248</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Accueil</a>
                    @else
                        <a href="{{ route('login') }}">Se connecter</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">S'inscrire</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Mars 2248
                </div>

                <div class="links">
                    <a href="/recherche">Minerais</a>
                    <a href="/admin">Vaisseau spatial Voyager</a>
                    <a href="https://fr.wikipedia.org/wiki/Histoire_de_l%27observation_de_Mars">News sur Mars</a>
                    <a href="https://blogs.letemps.ch/pierre-brisson/2019/04/06/produire-sa-nourriture-sur-mars-est-un-defi-quon-peut-envisager-des-maintenant-de-relever/">Blog Martien</a>
                    <a href="https://www.huffingtonpost.fr/entry/starship-elon-musk-fusee-spacex-coloniser-mars_fr_5d91b773e4b0019647ab157d">Notre sauveur</a>
                    <a href="https://gitlab.com/thomas314/simplonmars">GitMars</a>
                </div>
            </div>
        </div>
    </body>
</html>
