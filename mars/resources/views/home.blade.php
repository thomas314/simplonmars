@extends('layouts.app')

@section('content')
<div class="container d-flex">
    <h4>Vous recherchez une zone? Regardez si cette zone comprend un des 3 minerais principaux en tapant le nom du minerai.
      <br>Via ce minerai vous pourrez apercevoir la carte de notre si belle planète... et voir si il y a des minerais dangereux dans votre zone d'habitation.
    </h4>
    <a class="btn btn-dark justify-content-center" href="/recherche" role="button">Annuaire de nos zones enregistrées</a>
</div>
<div class="container">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="/img/minerai1.jpeg" height="500px" width="70px" alt="Slide 1">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="/img/minerai2.jpg" height="500px" width="100px" alt="Slide 2">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="/img/minerai3.jpg" height="500px" width="100px" alt="Slide 3">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Précédent</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Suivant</span>
        </a>
      </div>
</div>
@endsection
@extends('layouts.footer')
