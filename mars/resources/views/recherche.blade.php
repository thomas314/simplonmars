@extends('layouts.app')
@section('content')

<div class="container d-flex justify-content-end" >
    <form method="post" action="/findSearch">
        <input type="text" name="search">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button>Rechercher</button>
    </form>
</div>


@if (isset($details))


    <div class="container">

     @foreach($details as $mineral)
        <div class="row">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{ $mineral->name }}</h5>
                        <p class="card-text">{{$mineral->resume}}</p>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">En savoir plus</button>

                        <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Minerai recherché</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-profil">
                                        <h3>{{$mineral->name}}</h3>
                                <div class="container">
                                <p>{{$mineral->resume}}</p>
                                <hr>
                                <p>Dangeurosité: {{$mineral->dangerosity}} /10</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                    <a href="/maps" class="btn btn-primary stretched-link">Vers maps</a>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
@endsection
@extends('layouts.footer')