@extends('layouts.app')
@section('content')
<form action="/add_zones" method="POST">
  @csrf
  <p>Vous avez découvert une nouvelle zone dangereuse? Remplissez ce formulaire ! 
    <br>Nos chercheurs et scientifiques s'occuperont de valider vos informations.
  </p>
  Nom: <input type="text" name="title" id="title" placeholder="Nom de la zone" required>
  Résumé: <input type="text" name="resume" id="resume" placeholder="Résumé" required>
  Informations complémentaires: <input type="text" name="commentaire" id="commentaire" placeholder="Informations complémentaires">
  Dangeurosité: <input type="number" name="dangerosity" id="dangerosity" max="10" min="1" placeholder="1" required>
  <button class="btn btn-info" type="submit">Soumettre</button>
  
</form>
@if(isset($zones))
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nom</th>
      <th scope="col">Résumé</th>
      <th scope="col">Dangeurosité</th>
    </tr>
  </thead>
  @foreach($zones as $zone)
  <tbody>
    <tr>
      <th scope="row">{{ $zone->id }}</th>
      <td>{{ $zone->title }}</td>
      <td>{{ $zone->resume }}</td>
      <td>{{ $zone->dangerosity }}</td>
    </tr>
  </tbody>
  @endforeach
</table>
@endif
@endsection