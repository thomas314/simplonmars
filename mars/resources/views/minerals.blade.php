@extends('layouts.app')
@section('content')

<form action="/add_minerals" method="POST">
  @csrf
  <p>Vous avez découvert un nouveau minerai? Remplissez ce formulaire ! 
    <br>Nos chercheurs et scientifiques s'occuperont de valider vos informations
  </p>
  Nom: <input type="text" name="name" id="name" placeholder="Nom du minerai" required>
  Résumé: <input type="text" name="resume" id="resume" placeholder="Résumé" required>
  Informations complémentaires: <input type="text" name="commentaire" id="commentaire" placeholder="Informations complémentaires">
  Dangeurosité: <input type="number" name="dangerosity" id="dangerosity" max="10" min="1" placeholder="1" required>
  <button class="btn btn-info" type="submit">Soumettre</button>
  
</form>
@if(isset($minerals))
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nom</th>
      <th scope="col">Résumé</th>
      <th scope="col">Dangeurosité</th>
    </tr>
  </thead>
  @foreach($minerals as $mineral)
  <tbody>
    <tr>
      <th scope="row">{{ $mineral->id }}</th>
      <td>{{ $mineral->name }}</td>
      <td>{{ $mineral->resume }}</td>
      <td>{{ $mineral->dangerosity }}</td>
    </tr>
  </tbody>
  @endforeach
</table>
@endif
@endsection