@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Paramètres</h2>
    <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Modifier ses paramètres utilisateurs</div>
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="/param_user" onsubmit="return confirm('Vous êtes sur le point de modifier vos informations personnelles, cela vous convient ?')">
                                @csrf
                                <div class="form-group">
                                    <label for="name" class="col-md-4 control-label">Nom</label>
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" required autofocus>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-md-4 control-label">E-Mail</label>
                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Enregistrer
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    <div class="container">
        <p>Supprimer mon compte :</p>
        <form class="col-md-8 col-md-offset-4" action="/del_user" method="POST" onsubmit="return confirm('Êtes vous certain de vouloir supprimer votre compte utilisateur ? Cette action est irréversible !')">
            @csrf
            <button class="btn btn-danger" type="submit">Confirmer</button>
        </form>
    </div>
    </div>
@endsection
@extends('layouts.footer')