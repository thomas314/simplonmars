<footer class="fixed-bottom bg-light shadow-lg">
        <div class="container d-flex justify-content-around">
            <li class="nav-item btn"><a href="https://www.service-public.fr/professionnels-entreprises/vosdroits/F31228">Mentions Légales</a></li>
            <li class="nav-item btn"><a href="#">CGU</a></li>
            <li class="nav-item btn"><a href="#">CGV</a></li>
            <li class="nav-item btn"><a href="#">Contact</a></li>
            <li class="nav-item btn"><a href="#">facebook</a></li>
            <li class="nav-item btn"><a href="#">twitter</a></li>
            <li class="nav-item btn"><a href="#">linkedin</a></li>
        </div>
        <!-- Copyright -->
        <div class="footer-copyright text-center py-3 bg-dark text-light">© 2019 Copyright:
            <a href="/">Mars</a>
        </div>
    </footer>